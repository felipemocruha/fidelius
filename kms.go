package main

import (
	"context"

	kms "cloud.google.com/go/kms/apiv1"
)

type Key interface {
	Encrypt(payload []byte) ([]byte, error)
	Decrypt(payload []byte) ([]byte, error)
}

type KMS interface {
	CreateKey() (Key, error)
	GetKey(keyID string) (Key, error)
	RotateKey(keyID string) error
	DeleteKey(keyID string) error
}

type GoogleKMS struct {
	kmc *kms.KeyManagementClient
}

func NewGoogleKMS() (*GoogleKMS, error) {
	ctx := context.Background()
	gkms := &GoogleKMS{}

	client, err := cloudkms.NewKeyManagementClient(ctx)
	if err != nil {
		return nil, err
	}
	gkms.kmc = client

	return gkms, nil
}

func (k GoogleKMS) CreateKey() (Key, error) {

}

func (k GoogleKMS) GetKey(keyID string) (Key, error) {

}

func (k GoogleKMS) RotateKey(keyID string) error {

}

func (k GoogleKMS) DeleteKey(keyID string) error {

}
