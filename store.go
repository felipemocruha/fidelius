package main

import (
	"io"

	"cloud.google.com/go/storage"
)

type StoreOpts struct {
	OverwriteFiles bool
}

type Store interface {
	GetFile(name string) (io.Reader, error)
	CreateFile(name string, body []byte, opts ...StoreOpts) error
	DeleteFile(name string) error
}

type GCSstore struct {
	client *storage.Client
}

func (s GCSstore) GetFile(name string) (io.Reader, error) {

}

func (s GCSstore) CreateFile(name string, body []byte, opts ...StoreOpts) error {

}

func (s GCSstore) DeleteFile(name string) error {

}
