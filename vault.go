package main

import ()

type Vault interface {
	CreateSecret(name string, payload []byte) error
	GetSecret(name string) ([]byte, error)
	UpdateSecret(name, newPayload []byte) error
	DeleteSecret(name string) error
}

type VaultImpl struct {
	KeyID string
	KMS   KMS
	Store Store
}

func NewVault(keyID string, kms KMS) (*VaultImpl, error) {
	return &VaultImpl{
		KeyID: keyID,
		KMS:   kms,
	}
}

func (v VaultImpl) CreateSecret(name string, payload []byte) error {
	key, err := v.KMS.GetKey(v.keyID)
	if err != nil {
		return err
	}

	encrypted, err := key.Encrypt(payload)
	if err != nil {
		return err
	}

	if err = v.Store.CreateFile(name, encrypted); err != nil {
		return err
	}

	return nil
}

func (v VaultImpl) GetSecret(name string) ([]byte, error) {
	key, err := v.KMS.GetKey(v.keyID)
	if err != nil {
		return nil, err
	}

	secret, err := v.Store.GetFile(name)
	if err != nil {
		return nil, err
	}

	val, err := key.Decrypt(secret)
	if err != nil {
		return nil, err
	}

	return val, nil
}

func (v VaultImpl) UpdateSecret(name, newPayload []byte) error {
	key, err := v.KMS.GetKey(v.keyID)
	if err != nil {
		return err
	}

	encrypted, err := key.Encrypt(newPayload)
	if err != nil {
		return err
	}

	if err = v.Store.CreateFile(name, encrypted, StoreOpts{true}); err != nil {
		return err
	}

	return nil
}

func (v VaultImpl) DeleteSecret(name string) error {
	return v.Store.DeleteFile(name)
}
